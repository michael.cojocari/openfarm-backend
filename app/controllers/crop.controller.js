const db = require("../models");
const Crop = db.crops;
const dbUtility = require("../db.utility");

exports.findAllByPage = (req, res) => {
  var condition = dbUtility.buildConditions(req.query);

  let page = req.params.pageId >= 1 ? req.params.pageId : 1;
  const resultsPerPage = process.env.RESULTS_PAGE_LIMIT || 10;

  Crop.find(condition)
    .sort({ binomial_name: "asc" })
    .limit(resultsPerPage)
    .skip(resultsPerPage * page)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving crops.",
      });
    });
};

exports.findAll = (req, res) => {
  let condition = dbUtility.buildConditions(req.query);

  Crop.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving crops.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Crop.findById(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Could not find crop with id: " + id });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving crop with id " + id + ": " + err.message,
      });
    });
};
