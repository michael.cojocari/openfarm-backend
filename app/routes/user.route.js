module.exports = (app) => {
  const users = require("../controllers/user.controller");
  var router = require("express").Router();

  router.post("/register", users.register);
  router.put("/changepassword", users.changePassword);
  router.post("/login", users.login);
  router.get("/list", users.findAll);
  router.get("/details", users.getUserDetails);

  app.use("/api/auth", router);
};
