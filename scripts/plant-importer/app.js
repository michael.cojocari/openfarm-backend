const { MongoClient, ObjectId } = require('mongodb');
const fs = require('fs');

global.plantsToInsert = [];

const main = async () => {
    const mongodbUri = "mongodb://127.0.0.1:27017/openfarm";
    const mongoClient = new MongoClient(mongodbUri);

    try {
        await mongoClient.connect();
        console.log("Connected!");

        let plantsToInsert = await getPlantsToInsert();

        for(const plant of plantsToInsert) {
            const insertResult = await mongoClient.db("openfarm").collection("plants").insertOne(plant);
            // console.log("ID: " + (await insertResult).insertedId);
        }


    } finally {
        await mongoClient.close();
        console.log("Goodbye!");
    }
}

const getPlantsToInsert = async (client) => {
    let plantsToInsert = [];
    const files = fs.readdirSync('./plants-csv');
        // Go through each file
    files.forEach((file) => {
        // Read file
        let contents = fs.readFileSync('./plants-csv/' + file, 'utf-8');
         // Go through each plant in file
        let plants = contents.split('\n');
        plants.forEach((plant) => {
            plant = plant.replaceAll('\"', '');
            plant = plant.replaceAll('\r', '');
            plant = plant.split(",");
            if (plant[0] !== "Symbol") {
                let newPlantRecord = {
                    symbol: plant[0],
                    synonym_symbol: plant[1],
                    scientific_name_with_author: plant[2],
                    state_common_name: plant[3],
                    family: plant[4],
                    state: file.replace('.csv', ''),
                    plant_source: ObjectId.createFromHexString('6536f114c5b620de7ed42ff0')
                }
                plantsToInsert.push(newPlantRecord);
            }
        })
    })

    return plantsToInsert;
}

main().catch(console.error);

