module.exports = (mongoose) => {
  return mongoose.model(
    "gardens",
    mongoose.Schema({
      is_private: Boolean,
      user_id: mongoose.Types.ObjectId,
      name: String,
      description: String,
      average_sun: String,
      location: String,
      ph: Number,
      soil_type: String,
      type: String,
    }),
  );
};
