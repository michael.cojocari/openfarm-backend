module.exports = (mongoose) => {
  return mongoose.model(
    "crops",
    mongoose.Schema(
      {
        _id: [mongoose.Types.ObjectId],
        _keywords: [String],
        _slugs: [String],
        binomial_name: String,
        common_names: [String],
        created_at: Date,
        updated_at: Date,
        crop_data_source_id: [mongoose.Types.ObjectId], // Is this the right type to reference another model?
        guides_count: Number,
        impressions: Number,
        name: String,
      },
      { timestamps: false },
    ),
  );
};
