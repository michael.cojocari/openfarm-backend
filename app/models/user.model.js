module.exports = (mongoose) => {
  return mongoose.model(
    "users",
    mongoose.Schema({
      username: {
        type: String,
        unique: true,
        required: true,
      },
      password: {
        type: String,
        minLength: 6,
        required: true,
      },
      role: {
        type: String,
        default: "User",
        required: true,
      },
    }),
  );
};
