module.exports = (app) => {
  const plants = require("../controllers/plant.controller");
  var router = require("express").Router();

  router.get("/", plants.findAll);
  router.get("/:id", plants.findOne);
  router.get("/page/:pageId", plants.findAllByPage);

  router.get("/search", plants.search);

  app.use("/api/plants", router);
};
