const db = require("../models");
const Garden = db.gardens;
const User = db.users;
const dbUtility = require("../db.utility");
const authenticationUtility = require("../auth.utility");

exports.findAllByPage = (req, res) => {
  var condition = dbUtility.buildConditions(req.query);

  let page = req.params.pageId >= 1 ? req.params.pageId : 1;
  const resultsPerPage = process.env.RESULTS_PAGE_LIMIT || 10;

  Garden.find(condition)
    .sort({ name: "asc" })
    .limit(resultsPerPage)
    .skip(resultsPerPage * page)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving gardens.",
      });
    });
};

exports.findAll = (req, res) => {
  var condition = dbUtility.buildConditions(req.query);

  Garden.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving gardens.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Garden.findById(id)
    .then((data) => {
      if (!data) {
        res
          .status(404)
          .send({ message: "Could not find garden with id: " + id });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving garden with id " + id + ": " + err.message,
      });
    });
};

exports.delete = async (req, res) => {
  const gardenId = req.params.id;
  const token = req.headers.authorization.split(" ")[1];
  const garden = Garden.findById(gardenId);
  const user = User.findById(garden.user_id);

  if (!authenticationUtility.isAuthenticUser(user.username, token)) {
    return res.status(400).json({
      message: "Hey, you can't update this garden!",
    });
  }

  Garden.findByIdAndDelete(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete garden.`,
        });
      } else {
        res.send({
          message: "Garden was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.update = async (req, res) => {
  if (!req.body) {
    return res
      .status(400)
      .json({ message: "Garden must have a body of some sort." });
  }

  const token = req.headers.authorization.split(" ")[1];
  const user = await User.findById(req.body.user_id);

  if (!authenticationUtility.isAuthenticUser(user.username, token)) {
    return res.status(400).json({
      message: "Hey, you can't update this garden!",
    });
  }

  Garden.findByIdAndUpdate(req.body._id, req.body, { userFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: "Yo, could not update.",
        });
      } else {
        req.status(200).send(data);
      }
    })
    .catch((error) => {
      res.status(500).send({
        message: error.message || "Yo, could not update.",
      });
    });
};

exports.create = async (req, res) => {
  if (!req.body.name) {
    return res.status(400).json({ message: "Garden must have a name." });
  }

  const token = req.headers.authorization.split(" ")[1];
  const user = await User.findById(req.body.user_id);

  if (!authenticationUtility.isAuthenticUser(user.username, token)) {
    return res.status(400).json({
      message: "Hey, you can't create a garden.",
    });
  }

  User.findOne({ username: username }).then((user) => {
    const garden = new Garden({
      user_id: req.body.user_id,
      is_private: req.body.is_private,
      name: req.body.name,
      description: req.body.description || "",
    });
    garden
      .save()
      .then((garden) => {
        res.send(garden);
      })
      .catch((err) => {
        res.status(500).send({ message: err.message });
      });
  });
};
