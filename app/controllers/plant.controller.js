const db = require("../models");
const Plant = db.plants;
const dbUtility = require("../db.utility");

exports.findAllByPage = (req, res) => {
  var condition = dbUtility.buildConditions(req.query);

  let page = req.params.pageId >= 1 ? req.params.pageId : 1;
  const resultsPerPage = process.env.RESULTS_PAGE_LIMIT || 10;

  Plant.find(condition)
    .sort({ state_common_name: "asc" })
    .limit(resultsPerPage)
    .skip(resultsPerPage * page)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving plants.",
      });
    });
};

exports.findAll = (req, res) => {
  var condition = dbUtility.buildConditions(req.query);

  Plant.find(condition)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving plants.",
      });
    });
};

exports.findOne = (req, res) => {
  const id = req.params.id;

  Plant.findById(id)
    .then((data) => {
      if (!data) {
        res
          .status(404)
          .send({ message: "Could not find plant with id: " + id });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving plant with id " + id + ": " + err.message,
      });
    });
};

exports.search = (req, res) => {};
