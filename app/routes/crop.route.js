module.exports = (app) => {
  const crops = require("../controllers/crop.controller");
  var router = require("express").Router();

  router.get("/", crops.findAll);
  router.get("/:id", crops.findOne);
  router.get("/page/:pageId", crops.findAllByPage);

  app.use("/api/crops", router);
};
