require("dotenv").config();

const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8088",
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/shup", (req, res) => {
  res.json({ message: "Shup" });
});

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

require("./app/routes/crop.route")(app);
require("./app/routes/plant.route")(app);
require("./app/routes/user.route")(app);
require("./app/routes/garden.route")(app);

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log("App is running on port " + PORT);
});
