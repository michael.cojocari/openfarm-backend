exports.buildConditions = (query) => {
  var condition = {};
  Object.keys(query).forEach((key) => {
    condition[key] = { $regex: new RegExp(query[key]), $options: "i" };
  });

  return condition;
};
