const db = require("../models");
const User = db.users;
const bcrypt = require("bcryptjs");
const authenticationUtility = require("../auth.utility");

exports.findAll = (req, res) => {
  User.find()
    .select(["username", "role"])
    .sort({ username: "asc" })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message,
      });
    });
};

exports.getUserDetails = async (req, res) => {
  let user = null;
  if (req.query.user_id) {
    user = await authenticationUtility.getUserDetailsById(req.query.user_id, res);
  } else if (req.query.username) {
    user = await authenticationUtility.getUserDetailsByUsername(req.query.username, res);
  }

  const isAuthentic = await authenticationUtility.isAuthenticUser(
    user._id,
    req.headers.authorization,
  );

  if (!isAuthentic) {
    return res.status(400).json({
      message: "Hey, you can't see this user.",
    });
  }

  res.send(user);
};

exports.login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).json({
      message: "Username and Password required to login.",
    });
  }

  const user = await User.findOne({ username });
  if (user) {
    bcrypt.compare(password, user.password).then((result) => {
      if (result) {
        const authenticationToken =
          authenticationUtility.generateAuthenticationToken(username);
        res.json({
          username: username,
          authentication_token: authenticationToken,
        });
      } else {
        res.status(400).json({ message: "Login not succesful" });
      }
    });
  } else {
    res.status(400).json({
      message: "User not found.",
    });
  }
};

exports.changePassword = async (req, res) => {
  const { userId, password } = req.body;

  if (!password) {
    return res.status(400).json({
      message: "Dude, you're missing a password or a token.",
    });
  }

  if (password.length < 6) {
    return res
      .status(400)
      .json({ message: "Password should be longer than 6 characters." });
  }

  const isAuthentic = await authenticationUtility.isAuthenticUser(
    userId,
    req.headers.authorization,
  );

  if (isAuthentic) {
    const user = await authenticationUtility.getUserDetailsById(userId, res);

    bcrypt.hash(password, 10).then((hash) => {
      user.password = hash;
      User.findByIdAndUpdate(user._id, user, { useFindAndModify: false })
        .then((updatedUser) => {
          res.status(200).json({ message: "User password changed." });
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message,
          });
        });
    });
  } else {
    return res.status(400).json({
      message: "Hey you, you're not authentic.",
    });
  }
};

exports.register = (req, res) => {
  const { username, password } = req.body;
  if (password.length < 6) {
    return res
      .status(400)
      .json({ message: "Password should be longer than 6 characters." });
  }

  bcrypt.hash(password, 10).then((hash) => {
    User.create({
      username,
      password: hash,
    })
      .then((user) => {
        res.status(200).json({ message: "User created succesfully." });
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message,
        });
      });
  });
};
