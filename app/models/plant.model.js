module.exports = (mongoose) => {
  return mongoose.model(
    "plants",
    mongoose.Schema(
      {
        _id: [mongoose.Types.ObjectId],
        symbol: String,
        synonym_symbol: String,
        family: String,
        plant_source: [mongoose.Types.ObjectId],
        scientific_name_with_author: String,
        state: String,
        state_common_name: String,
      },
      { timestamps: false },
    ),
  );
};
