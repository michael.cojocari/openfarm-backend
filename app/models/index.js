const dbConfig = require("../config/db.config");
const mongoose = require("mongoose");
const { mongo } = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.HOST;

db.crops = require("./crop.model.js")(mongoose);
db.plants = require("./plant.model.js")(mongoose);
db.users = require("./user.model.js")(mongoose);
db.gardens = require("./garden.model.js")(mongoose);

module.exports = db;
