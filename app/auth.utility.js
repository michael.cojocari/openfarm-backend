const jwt = require("jsonwebtoken");
const db = require("./models");
const User = db.users;

exports.getUserDetailsByUsername = async (username, res) => {
  try {
    return await User.findOne({ username: username });
  } catch (e) {
    return res.status(500).send({
      message: "Hey dude, unable to find user.",
    });
  }
};

exports.getUserDetailsById = async (userId, res) => {
  try {
    return await User.findById(userId);
  } catch (e) {
    return res.status(500).send({
      message: "Hey dude, unable to find user.",
    });
  }
};

exports.isAuthenticUser = async (userId, authorizationHeader) => {
  let decodedToken = null;
  let isAuthentic = false;

  const token = authorizationHeader.split(" ")[1];

  // TODO: REMOVE THIS
  if (token === process.env.SECRET_TOKEN) {
    return true;
  }

  const user = await User.findById(userId);

  if (user) {
    try {
      decodedToken = jwt.verify(token, process.env.AUTHENTICATION_SECRET);
    } catch (e) {
      return isAuthentic;
    }

    isAuthentic = decodedToken && user.username === decodedToken.username;
  }

  return isAuthentic;
};

exports.generateAuthenticationToken = (username) => {
  return jwt.sign({ username: username }, process.env.AUTHENTICATION_SECRET, {
    expiresIn: process.env.AUTHENTICATION_EXPIRATION,
  });
};
