module.exports = (app) => {
  const gardens = require("../controllers/garden.controller");
  let router = require("express").Router();

  router.get("/", gardens.findAll);
  router.get("/:id", gardens.findOne);
  router.get("/page/:pageId", gardens.findAllByPage);
  router.post("/", gardens.create);
  router.put("/", gardens.update);
  router.delete("/:id", gardens.delete);

  app.use("/api/gardens", router);
};
